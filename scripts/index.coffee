style = document.createElement('style')
style.setAttribute('type', 'text/css')
document.head.appendChild(style)

class Anime
    constructor: () ->
        @title = ko.observable('')
        @status = ko.observable('')
        @rating = ko.observable(5)

class ViewModel
    constructor: () ->
        @username = ko.observable('')
        @email = ko.observable('')
        @password = ko.observable('')

        @working_anime = new Anime()

        @planned_anime = ko.observableArray()
        @in_progress_anime = ko.observableArray()
        @complete_anime = ko.observableArray()
        @dropped_anime = ko.observableArray()

        @page_title = ko.computed =>
            if @username() == ''
                return 'listed.moe'
            else
                return @username() + "'s Anime"
        
        @user_css = ko.computed({
            read: =>
                return style.textContent
            write: (value) =>
                style.textContent = value
        })

        @rate_anime = (anime, rating) =>
            anime.rating(rating)

window.viewModel = new ViewModel()
ko.applyBindings(window.viewModel)

# Set up typeahead for anime titles/nicknames
engine = new Bloodhound({
    name: 'anime',
    remote: '/api/anime/title?query=%QUERY',
    datumTokenizer: (d) -> [d.title].concat(d.alternates)
    queryTokenizer: Bloodhound.tokenizers.whitespace
})
engine.initialize()

$("#working-anime-title").typeahead({
    minLength: 0,
    highlight: true
}, { name: 'anime', source: engine.ttAdapter(), displayKey: 'title' })


# Set up CSS editor
editor = ace.edit("user-css")
editor.setTheme("ace/theme/eclipse")
editor.getSession().setMode("ace/mode/css")
editor.getSession().on('change', (e) ->
    viewModel.user_css(editor.getValue())
)
