from sqlalchemy.sql import exists
from listed.objects import Anime, AnimeTitle
from listed.database import db
import xml.etree.ElementTree as ET

tree = ET.parse('anime-titles.xml')
root = tree.getroot()

for anime in root:
    canonical_title = None
    titles = set()
    for t in anime:
        if t.attrib['type'] == 'official' and t.attrib['{http://www.w3.org/XML/1998/namespace}lang'] == 'en':
            canonical_title = t.text
        if t.attrib['{http://www.w3.org/XML/1998/namespace}lang'] in ['en', 'ja', 'x-jat']:
            titles.update([t.text])
    if len(titles) == 0:
        continue
    if not canonical_title:
        canonical_title = next(iter(titles))
    aid = int(anime.attrib['aid'])
    if db.query(exists().where(Anime.anidb_id == aid)).scalar():
        print("Skipping {0}".format(canonical_title))
        continue

    a = Anime()
    a.title = canonical_title
    a.anidb_id = aid
    db.add(a)
    for title in titles:
        t = AnimeTitle()
        t.title = title
        t.anime = a
        db.add(t)
        a.titles.append(t)
    db.commit()
    print("Added {0}".format(canonical_title))
