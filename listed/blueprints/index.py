from flask import Blueprint, render_template, abort, request, redirect, session, url_for
from flask.ext.login import current_user, login_user
from sqlalchemy import desc
from shutil import move
from datetime import datetime
from listed.objects import *
from listed.common import *
from listed.config import _cfg

import os
import zipfile
import urllib
import tempfile

index = Blueprint('index', __name__, template_folder='../../templates')

@index.route("/")
def index_page():
    return render_template("index.html")
